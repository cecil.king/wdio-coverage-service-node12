[![pipeline status](https://gitlab.com/archsoft/wdio-coverage-Service/badges/master/pipeline.svg)](https://gitlab.com/archsoft/wdio-coverage-Service/-/commits/master)
[![coverage report](https://gitlab.com/archsoft/wdio-coverage-Service/badges/master/coverage.svg)](https://gitlab.com/archsoft/wdio-coverage-Service/-/commits/master)

WDIO COVERAGE Service
===================

A WebdriverIO plugin. Extracts coverage information.



## FAQ

### Limitations:
- currently coverage results aren't separated e.g. running chrome, firefox in parallel would result in a merged coverage result out of both runs

### Why are't you using [devtools-service](https://webdriver.io/docs/devtools-service#capture-code-coverage):
- If you are using a web-framework made of large files, you can't exclude them. Which makes the test very slow
- Works only in chrome
- Wasn't available when this plugin was written
### Why is this not on Github
Github actions have not been a available.

### About this fork

This was forked from the [original wdio-coverage-service](https://gitlab.com/archsoft/wdio-coverage-service) in order to handle limitations of node v12 and npm v6.
Specifically, `fs.rmSync` is not available in node v12. It was replaced with `fs.rmdirSync` which should be adequate.
In npm versions 3 through 6, `peerDependencies` are not installed automatically. To workaround this, `istanbul-lib-coverage` was changed to a dependency.

## WDIO Version Compatibility

The chart below shows the versions of this Service and their WDIO compatibility version.

| WDIO Coverage Service | WDIO |
| ------------------ | ---- |
| ^0.0.1             | v6  (sync-mode) |
| ^1.0.x             | v7  (async) |



# WDIO v6 Compatibility

## Installation

* NPM
```bash
npm install wdio-coverage-service --save-dev
```

```js
Services: [
  'dot',
  'coverage'
],
```

## Configuration
This plugin is fetching the test coverage information after each suit/scenario into `.nyc_output`.
 - The page under test must be instrumented. Check [NYC](https://www.npmjs.com/package/nyc).
 - The report must be generated with [NYC](https://www.npmjs.com/package/nyc), after the WDIO run.


An example with instrumented code and static server can be found in `test/integration`. 
Usually, the framework of your choice has already middleware for serving and instrumenting on the fly.

Example(please add more with PR): 
- https://github.com/istanbuljs/babel-plugin-istanbul


Example:
```js
{
"scripts": {
    "test": "npm run-script instrument && npm run-script wdio && nyc report",
    "instrument": "nyc instrument ./webapp ./dist --complete-copy --delete",
    "wdio": "npx wdio wdio.conf.js",
    "cover-report": "nyc report"
  },
"nyc": {
    "all": true,
    "check-coverage": false,
    "exclude": [],
    "extension": [
      "*.js"
    ],
    "reporter": [
      "json",
      "text",
      "lcov"
    ]
  }
}
```


For more information on WebdriverIO see the [homepage](http://webdriver.io).
