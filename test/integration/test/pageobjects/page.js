/**
* main page object containing all methods, selectors and functionality
* that is shared across all page objects
*/
class Page {
  /**
    * Opens a sub page of the page
    * @param path path of the sub page (e.g. /path/to/page.html)
    */
  async open()  {
    await browser.url('/dist')
  }

  async pressButton() {
    const elem = await $('#myButton')
    await elem.click()
  }
}
module.exports = new Page()
